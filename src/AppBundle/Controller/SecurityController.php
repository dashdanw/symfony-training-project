<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SecurityController extends Controller
{
    /**
     * This action displays the login form.
     *
     * @Route("/login", name="app_login")
     * @Method("GET")
     */
    public function loginAction()
    {
        return $this->render('security/login.html.twig');
    }

    /**
     * This action handles the submitted credentials check.
     *
     * @Route("/login", name="app_login_check")
     * @Method("POST")
     */
    public function loginCheckAction()
    {
        
    }

    /**
     * This action disconnects the authenticated user.
     *
     * @Route("/logout", name="app_logout")
     * @Method("GET")
     */
    public function logoutAction()
    {
        
    }
}
