<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/game")
 */
class GameController extends Controller
{
    /**
     * @Route("/", name="app_game_play")
     * @Method("GET")
     */
    public function playAction()
    {
        $runner = $this->get('app.game_runner');

        return $this->render('game/play.html.twig', [
            'game' => $runner->loadGame(),
        ]);
    }

    /**
     * @Route(
     *   path="/play/{letter}",
     *   name="app_game_play_letter",
     *   requirements={
     *     "letter"="[a-z]"
     *   }
     * )
     * @Method("GET")
     */
    public function playLetterAction($letter)
    {
        $runner = $this->get('app.game_runner');
        $game = $runner->playLetter($letter);

        if (!$game->isOver()) {
            return $this->redirectToRoute('app_game_play');
        }

        return $this->redirectToRoute($game->isWon() ? 'app_game_win' : 'app_game_lose');
    }

    /**
     * @Route(
     *   path="/play",
     *   name="app_game_play_word",
     *   condition="request.request.get('word') matches '/^[a-z]+$/i'"
     * )
     * @Method("POST")
     */
    public function playWordAction(Request $request)
    {
        $runner = $this->get('app.game_runner');
        $game = $runner->playWord($request->request->get('word'));

        return $this->redirectToRoute($game->isWon() ? 'app_game_win' : 'app_game_lose');

    }

    /**
     * @Route("/reset", name="app_game_reset")
     * @Method("GET")
     */
    public function resetAction()
    {
        $this->get('app.game_runner')->resetGame();

        return $this->redirectToRoute('app_game_play');
    }

    /**
     * @Route("/win", name="app_game_win")
     * @Method("GET")
     */
    public function winAction()
    {
        $game = $this->get('app.game_runner')->resetGameOnSuccess();

        return $this->render('game/win.html.twig', [ 'game' => $game ]);
    }

    /**
     * @Route("/lose", name="app_game_lose")
     * @Method("GET")
     */
    public function loseAction()
    {
        $game = $this->get('app.game_runner')->resetGameOnFailure();

        return $this->render('game/lose.html.twig', [ 'game' => $game ]);
    }
}
