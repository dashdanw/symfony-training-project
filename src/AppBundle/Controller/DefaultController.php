<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/hi/{name}", name="app_hello")
     */
    public function indexAction($name = 'foooo')
    {
        return $this->render('default/hello.html.twig', [
            'first_name' => $name,
        ]);
    }
}
