<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class UserController extends Controller
{
    public function sidebarAction()
    {
        $users = [
            [ 'username' => 'fabien' ],
            [ 'username' => 'hugo' ],
            [ 'username' => 'jimmy' ],
            [ 'username' => 'claude' ],
            [ 'username' => 'pierre' ],
        ];

        return $this->render('user/sidebar.html.twig', array(
            'users' => $users,
        ));
    }
}
