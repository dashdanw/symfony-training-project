<?php

namespace AppBundle\Tests\Game;

use AppBundle\Game\WordList;

class WordListTest extends \PHPUnit_Framework_TestCase
{
    private static function readPrivateProperty($instance, $property)
    {
        $ro = new \ReflectionObject($instance);
        $rp = $ro->getProperty($property);
        $rp->setAccessible(true);

        return $rp->getValue($instance);
    }

    /**
     * @expectedException \RuntimeException
     * @dataProvider provideDictionary
     */
    public function testUnsupportedDictionaries($filename)
    {
        $wordList = new WordList();
        $wordList->loadDictionaries([ $filename ]);
    }

    public function provideDictionary()
    {
        return [
            [ 'foo.txt' ],
            [ 'bar.xml' ],
            [ 'toto.dat' ],
            [ 'qux.json' ],
        ];
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testUnsupportedWordLength()
    {
        $wordList = new WordList();
        $wordList->getRandomWord(5);
    }

    public function testAddSameWordTwice()
    {
        $wordList = new WordList();
        $wordList->addWord('datto');
        $wordList->addWord('datto');

        $words = self::readPrivateProperty($wordList, 'words');

        $this->assertCount(1, $words);
        $this->assertArrayHasKey(5, $words);
        $this->assertCount(1, $words[5]);
        $this->assertSame([ 'datto' ], $words[5]);
    }

    public function testAddSeveralWords()
    {
        $wordList = new WordList();
        $wordList->addWord('datto');
        $wordList->addWord('dotta');
        $wordList->addWord('fooba');

        $words = self::readPrivateProperty($wordList, 'words');
        
        $this->assertArrayHasKey(5, $words);
        $this->assertCount(3, $words[5]);
        $this->assertSame([ 'datto', 'dotta', 'fooba' ], $words[5]);
        $this->assertRegExp('/^(?:datto|dotta|fooba)$/', $wordList->getRandomWord(5));
    }

    public function testAddSingleWord()
    {
        $wordList = new WordList();
        $wordList->addWord('datto');

        $this->assertSame('datto', $wordList->getRandomWord(5));
    }
}
