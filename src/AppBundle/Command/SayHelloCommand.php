<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class SayHelloCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('datto:hello')
            ->setDescription('Say hello to someone.')
            ->addArgument('name', InputArgument::REQUIRED, 'The name to greet')
            ->addOption('yell', 'y', InputOption::VALUE_NONE, 'Say hello in uppercase.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        $message = 'Hello '.$name.'!';
        if ($input->getOption('yell')) {
            $message = strtoupper($message);
        }

        $output->writeln($message);
    }

}
